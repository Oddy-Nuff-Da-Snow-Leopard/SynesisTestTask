package com.group.synesis.service1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.stream.IntStream;

@RestController
@RequestMapping("/producer")
public class ProducerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProducerController.class.getName());

    public static final String URL = "http://localhost:8080/consumer";

    @PostMapping
    public void callAnotherService(@RequestParam(name = "callTimes") int callTimes) {
        LOGGER.info(String.format("Calling service2 %d times", callTimes));
        RestTemplate restTemplate = new RestTemplate();
        IntStream.range(0, callTimes).forEachOrdered(i -> restTemplate.postForObject(URL, null, Void.class));
    }
}
