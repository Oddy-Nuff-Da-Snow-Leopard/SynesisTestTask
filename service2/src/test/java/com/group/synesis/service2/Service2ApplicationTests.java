package com.group.synesis.service2;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class Service2ApplicationTests {

    public static final String URL_TEMPLATE = "/consumer";

    public static final int CALL_AMOUNT = 10;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testSimplePost() {
        try {
            for (int i = 0; i < CALL_AMOUNT; i++) {
                mockMvc.perform(post(URL_TEMPLATE))
                        .andDo(print())
                        .andExpect(status().isOk());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testSimpleGet() {
        try {
            mockMvc.perform(get(URL_TEMPLATE))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
                    .andExpect(jsonPath("$.['127.0.0.1']").value(CALL_AMOUNT));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
