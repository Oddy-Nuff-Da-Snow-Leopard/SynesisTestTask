package com.group.synesis.service2.jmx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import java.util.HashMap;
import java.util.Map;

@ManagedResource
public class CallStatistic {

    public static final Logger LOGGER = LoggerFactory.getLogger(CallStatistic.class.getName());

    private Map<String, Integer> callMap;

    public CallStatistic() {
        callMap = new HashMap<>();
    }

    @ManagedOperation
    public Integer incrementCallAmount(String ipAddress) {
        callMap.putIfAbsent(ipAddress, 0);
        callMap.put(ipAddress, callMap.get(ipAddress) + 1);
        LOGGER.info("Incremented for IP:" + ipAddress + "; call amount = " + callMap.get(ipAddress));
        return callMap.get(ipAddress);
    }

    @ManagedOperation
    public Map<String, Integer> getCallMap() {
        return new HashMap<>(callMap);
    }
}