package com.group.synesis.service2.config;

import com.group.synesis.service2.jmx.CallStatistic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

//    @Bean
//    public MBeanExporter mbeanExporter(DynamicIpStatistic dynamicIpStatistic) {
//        MBeanExporter exporter = new MBeanExporter();
//        Map<String, Object> beans = new HashMap<>();
//        beans.put("com.group.synesis.service2:type=DynamicIpStatistic,name=Addresses", dynamicIpStatistic);
//        exporter.setBeans(beans);
//        return exporter;
//    }

    @Bean
    public CallStatistic ipStatistic() {
        return new CallStatistic();
    }
}
