package com.group.synesis.service2.controller;

import com.group.synesis.service2.jmx.DynamicCallStatistic;
import com.group.synesis.service2.jmx.CallStatistic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerController.class.getName());

    private final DynamicCallStatistic dynamicCallStatistic;
    private final CallStatistic callStatistic;

    public ConsumerController(DynamicCallStatistic dynamicCallStatistic, CallStatistic callStatistic) {
        this.dynamicCallStatistic = dynamicCallStatistic;
        this.callStatistic = callStatistic;
    }

    @PostMapping
    public void simplePost(HttpServletRequest request) {
        String ipAddress = request.getRemoteAddr();
        LOGGER.info("IP from which the request came: " + ipAddress);
        callStatistic.incrementCallAmount(ipAddress);
        dynamicCallStatistic.incrementCallAmount(ipAddress);
    }

    @GetMapping
    public Map<String, Integer> simpleGet() {
        return callStatistic.getCallMap();
    }
}
